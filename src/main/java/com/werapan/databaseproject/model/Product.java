/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.werapan.databaseproject.model;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Arthaphan
 */
public class Product {

    private int id;
    private String Name;
    private float price;
    private String sweetlevel;
    private String size;
    private String type;
    private int categoryId;

    public Product(int id, String Name, float price, String sweetlevel, String size, String type, int categoryId) {
        this.id = id;
        this.Name = Name;
        this.price = price;
        this.sweetlevel = sweetlevel;
        this.size = size;
        this.type = type;
        this.categoryId = categoryId;
    }

    public Product(String Name, float price, String sweetlevel, String size, String type, int categoryId) {
        this.id = -1;
        this.Name = Name;
        this.price = price;
        this.sweetlevel = sweetlevel;
        this.size = size;
        this.type = type;
        this.categoryId = categoryId;
    }

    public Product() {
        this.id = -1;
        this.Name = "";
        this.price = 0;
        this.sweetlevel = "";
        this.size = "";
        this.type = "";
        this.categoryId = 0;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return Name;
    }

    public void setName(String Name) {
        this.Name = Name;
    }

    public float getPrice() {
        return price;
    }

    public void setPrice(float price) {
        this.price = price;
    }

    public String getSweetlevel() {
        return sweetlevel;
    }

    public void setSweetlevel(String sweetlevel) {
        this.sweetlevel = sweetlevel;
    }

    public String getSize() {
        return size;
    }

    public void setSize(String size) {
        this.size = size;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public int getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(int categoryId) {
        this.categoryId = categoryId;
    }

    @Override
    public String toString() {
        return "Product{" + "id=" + id + ", Name=" + Name + ", price=" + price + ", sweetlevel=" + sweetlevel + ", type=" + type + ", categoryId=" + categoryId + '}';
    }

    public static Product fromRS(ResultSet rs) {
        Product product = new Product();
        try {
            product.setId(rs.getInt("product_id"));
            product.setName(rs.getString("product_name"));
            product.setPrice(rs.getFloat("product_price"));
            product.setSize(rs.getString("product_size"));
            product.setSweetlevel(rs.getString("product_sweet_level"));
            product.setCategoryId(rs.getInt("category_id"));

        } catch (SQLException ex) {
            Logger.getLogger(Product.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
        return product;
    }
}
